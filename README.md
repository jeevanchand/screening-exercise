# PS-get-deep-property exercise

## Instructions

- Using vanilla javascript, create a function that gets the value of a property at a given path
- Example:
  - If given the object: {person: {name: {first: 'FirstName', middleInitial: 'I', lastName: 'LastName''}}}
  - And given the path: 'person.name.lastName'
  - The output would be: 'LastName'
- After you complete the exercise, provide any notes on your code below such as how to run your example

## Candidate Notes:

1. First find the deepProperty.html.
2. Just double click on the file, then you can see the output in the alert box.


# UI-bar-graph exercise

## Instructions

- Using any UI framework (or none) create a bar graph using the data.json file provided using javascript and css
- Do not use any third party graph libraries, the graph should be your own
- After you complete the exercise, provide notes below on how your code can be ran whether it be by simply opening a index.html file or through an npm command

## Candidate Notes:

1. Can run this , by opening html file. 